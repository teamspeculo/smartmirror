var express  = require('express');
var app      = express();
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash 	 = require('connect-flash');

// Standard stuff for a web based app
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var path 		 = require('path');

var configDB = require('./config/database.js');
mongoose.connect(configDB.url);

app.use('/app', express.static('../client'));
app.use(express.static('../client/app'));

app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
 // get information from html forms

//Stuff that passport needs
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var User            = require('./models/user.js');


	//This is for the sessions supported by passport//////
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
    //////////////////////////////////////////////////////

    //Local signup
    passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, email, password, done) {
        process.nextTick(function() {

        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            if (user) {
                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            }
            else {
                var newUser            = new User();


                newUser.local.email    = email;
                newUser.local.password = newUser.generateHash(password);
                newUser.local.news = false;
                newUser.local.clock = false;
                newUser.local.weather = false;
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }

        });

        });

    }));


    //Local login
    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, email, password, done) {
        process.nextTick(function() {


        User.findOne({ 'local.email' :  email }, function(err, user) {
            if (err)
                return done(err);
            if (!user) {
                return done(null, false, req.flash('loginMessage', 'No user by that name'));
            }
            if (!user.validPassword(password)) {
            	return done(null, false, req.flash('loginMessage', 'Wrong password'));
            }

            return done(null, user);

         	});
    	});
    }));

app.use(flash());

// load up the mirror model
var Mirror = require('./models/mirrors.js');

//Offload the routing to another file
require('./config/routes.js')(app, passport, Mirror);

app.listen(port);
console.log('Listening on port ' + port);
