// load all the things we need
var passport = require('passport');
var LocalStrategy   = require('passport-local').Strategy;
console.log("Got strat");
// load up the user model
var User            = require('../models/user.js');

// expose this function to our app using module.exports
module.exports = function() {



    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });



    passport.use(new LocalStrategy({
       
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email, password, done) {

      
        process.nextTick(function() {

        User.findOne({ 'local.email' :  email }, function(err, user) {
       
            if (err)
                return done(err);

            if (user) {
                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

              
                var newUser            = new User();

          
                newUser.local.email    = email;
                newUser.local.clock = false;
                newUser.local.news = false;
                newUser.local.weather = false;

          
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }

        });    

        });

    }));

};