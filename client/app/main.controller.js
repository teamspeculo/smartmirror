angular
		.module('app')
		.controller('MainController', ["$scope", "User",

		function MainController($scope, User)
		{
			$scope.isLoggedIn = function()
			{
				if(User.logged_in)
				{
					return true;
				}
				else
				{
					return false;
				}
			}

		}]);
