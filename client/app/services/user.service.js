angular
		.module('app')
		.service('User', function() {
			this.logged_in = false;
			this.FirstName = 'test';
			this.setFirstName = function (name) {
				this.FirstName = name;
			}
			this.login = function () {
				console.log("logging in");
				this.logged_in = true;
			}
			this.logout = function () {
				console.log("logging out");
				
				this.logged_in = false;
			}
		});