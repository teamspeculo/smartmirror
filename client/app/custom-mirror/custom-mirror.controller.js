angular
		.module('app')
		.controller('CustomMirrorController', ["$scope", "$resource", "$http", "$location", "User",

		function CustomMirrorController($scope, $resource, $http, $location, User)
		{
			$scope.username = User.FirstName;

			$scope.news = false;
			$scope.weather = false;
			$scope.clock = false;

			$scope.checkboxModel = {
				weatheron : false,
				newson : false,
				clockon : false
			};

			$scope.save = function()
			{
				var newMirror = {
					user   : $scope.username,
					weather	 : {
						on   : $scope.checkboxModel.weatheron,
						xPos : $scope.weatherx,
						yPos : $scope.weathery
					},
					news		 : {
						on   : $scope.checkboxModel.newson,
						xPos : $scope.newsx,
						yPos : $scope.newsy
					},
					clock		 : {
						on   : $scope.checkboxModel.clockon,
						xPos : $scope.clockx,
						yPos : $scope.clocky
					}
				};

				console.log(newMirror);

				var URL = '/mirror/userupdate';
				var localRequest = {
					method: 'POST',
					url: URL,
					data: newMirror
				};

				//pickup data
				$http(localRequest)
					.then(function(res) {
						console.log(res);
					})
			}

			$scope.dragndrop = function() {
				var URL = '/mirror/getMirrors/' + User.FirstName;
				var localRequest = {
					method: 'GET',
					url: URL
				};

				//pickup data
				$http(localRequest).then(function(res) {
						var url = '/mirror/?serial=' + res.data[0].serial;
						window.location = url;
				})
			}
		}])

		angular.module('app').controller('ClaimMirrorController', ["$scope", "User", "$http",

				function ClaimMirrorController($scope, User, $http)
				{
					$scope.username = User.FirstName;
					$scope.save = function()
					{
						if($scope.serial != undefined) {
							var newMirror = {
								serial : $scope.serial,
								user : User.FirstName
							};

							var URL = '/mirror/update';
							var localRequest = {
								method: 'POST',
								url: URL,
								data: newMirror
							};

							//pickup data
							$http(localRequest)
								.then(function(res) {
									console.log(res);
								})

						}
					}


				}]);
