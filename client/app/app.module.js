angular
	.module('app', ['ui.bootstrap', 'ui.router', 'ngResource', 'laneolson.ui.dragdrop'])


	.config(function($stateProvider, $urlRouterProvider)
	{
		$urlRouterProvider.when('', '/');
		$stateProvider
		.state('home', {
			name: 'home',
			url: '/',
			templateUrl: 'views/partial-home.html'
			})

		.state('login', {
			name: 'login',
			url: '/login',
			views: {
				"" : {
					templateUrl: 'views/login-partial.html',
					controller: 'LoginController'
				}
			}
		})

		/*.state('logout', {
			name: 'logout',
			url: '/logout',
			views: {
				"" : {
					templateUrl: '',
					controller: 'LogoutController'
				}
			}
		})*/

		.state('claim-mirror', {
			name: 'claim-mirror',
			url: '/claim',
			views: {
				"" : {
					templateUrl: 'custom-mirror/partial-claim-mirror.html',
					controller: 'ClaimMirrorController'
				}
			}
		})

		.state('customize-mirror', {
			name: 'customize-mirror',
			url: '/customize',
			views: {
				"" : {
					templateUrl: 'custom-mirror/partial-customize-mirror.html',
					controller: 'CustomMirrorController'
				}
			}
		})

		.state('profile', {
			name: 'profile',
			url: '/profile',
			views: {
				"" : {
					templateUrl: 'views/profile.html',
					controller: 'ProfileController'
				}
			}
		})

		.state('signup', {
			name: 'signup',
			url: '/signup',
			views: {
				"" : {
					templateUrl: 'views/partial-signup.html',
					controller: 'SignupController'
				}
			}

			});
	});
