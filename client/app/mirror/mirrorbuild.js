
// DOM Ready =============================================================
$(document).ready(function() {

    $('#btnAddMirror').on('click', addMirror);
});

// Add User
function addMirror(event) {

    event.preventDefault();

    console.log($('input#weatheron').is(':checked'));

    // Super basic validation - increase errorCount variable if any fields are blank
    var errorCount = 0;
    $('#mirror-form input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });

    // Check and make sure errorCount's still at zero
    if(errorCount === 0) {

        // If it is, compile all user info into one object
        var newMirror = {
            'serial': $('input#serial').val(),
            'user': $('input#user').val(),
            'weather': {
              'on':  $('input#weatheron').is(':checked'),
              'xPos':  parseInt($('input#weatherx').val()),
              'yPos':  parseInt($('input#weathery').val())
            },
            'news': {
              'on':  $('input#newson').is(':checked'),
              'xPos':  parseInt($('input#newsx').val()),
              'yPos':  parseInt($('input#newsy').val())
            },
            'clock': {
              'on':  $('input#clockon').is(':checked'),
              'xPos':  parseInt($('input#clockx').val()),
              'yPos':  parseInt($('input#clocky').val())
            }
        };

        // Use AJAX to post the object to our adduser service
        $.ajax({
            type: 'POST',
            data: newMirror,
            url: '/mirror/create',
            dataType: 'JSON'
        }).done(function( response ) {

            // Check for successful (blank) response
            if (response.msg === '') {
              alert("created new mirror");
            }
            else {

                // If something goes wrong, alert the error message that our service returned
                alert('Error: ' + response.msg);

            }
        });

    }
    else {
        // If errorCount is more than 0, error out
        alert('Please fill in all fields');
        return false;
    }
};
